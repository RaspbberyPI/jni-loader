# JNI Loader
This library can load native library by name from JNI default location and in case if native library is missing, 
library will try extract native library from JAR fail. Also, if there is enabled file checksum comparing functionality
native library will be extracted from JAR only when checksum of file in lib is different from file in JAR.
Application read checksum from file in JAR lib_name.sha256 and calculate for file located in "lib" directory.
**Currently work only on Linux.**
### Required files for library extraction
* library.so
* library.sha256

### Functionality
* Load library from default JNI location
* Extract from JAR to "lib" folder("lib" folder will be created in working dir) and load. 

## Technologies
* Java 11
* Maven

## Built With
* [Maven](https://maven.apache.org/) - Dependency Management

### Compile application
```bash
./mvnw clean compile
```

### Install to local maven
```bash
./mvnw install
```

## API
* **create(String name)** - create loader for file {name}
* **extLibraryDir(Path path)** - *optional* - set folder(absolute or relative) where to store library if extract required. Default is **"lib"**
* **jarLibraryPath(String path)** - *optional* - set library location in JAR fil(classpath). Resource read through "getResourceAsStream". Default is **""**
* **checkFileChecksum(boolean check)** - *optional* - Enabling or disabling file checksum checking
* **load()** - Load library or throw NativeLoaderException

## Usage
### Simple usage
Using library with default values
```java NativeLoader
NativeLoader.create("lib_name")
            .load();
```
### Advance usage
You can also override couple parameters
```java NativeLoader
NativeLoader.create("lib_name")
            .extLibraryDir(Path.of("lib/folder")) // where to put library file , default is "lib"
            .jarLibraryPath("/eu/galjente/jni") // where to look for library file in jar, default is ""
            .checkFileChecksum(true) // Should application check sha256 and override if not equals, default true
            .load();
```

## Versioning
We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/RaspbberyPI/jni-loader/-/tags).

## License ![GitLab](https://img.shields.io/gitlab/license/RaspbberyPI/jni-loader)
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Authors
* **Arturs Cvetkovs** - *Initial work* - [Galjente](https://gitlab.com/RaspbberyPI/jni-loader)
