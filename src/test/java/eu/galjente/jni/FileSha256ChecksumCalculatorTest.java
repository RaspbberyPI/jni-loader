package eu.galjente.jni;

import org.junit.jupiter.api.Test;

import java.net.URL;

import static org.junit.jupiter.api.Assertions.*;

public class FileSha256ChecksumCalculatorTest {

    private static final String CHECKSUM_TEST_FILE_PATH = "/checksum_test.txt";
    private static final String SHA256_TEST_FILE_CHECKSUM = "8d06d077b9171658508b9d435d09f340e149a196f6a3f655ab8d33fcdb78e933";

    @Test
    public void shouldCalculateTestFileChecksum() throws Exception {
        final URL testFileUrl = getClass().getResource(CHECKSUM_TEST_FILE_PATH);
        assertNotNull(testFileUrl, "Test file \"" + CHECKSUM_TEST_FILE_PATH +"\" not found");
        try (FileSha256ChecksumCalculator calculator = new FileSha256ChecksumCalculator(testFileUrl.getFile())) {
            assertEquals(SHA256_TEST_FILE_CHECKSUM, calculator.calculate());
        }
    }

}