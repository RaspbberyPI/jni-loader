package eu.galjente.jni;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class NativeLoaderExceptionTest {

    private static final String ERROR_MESSAGE = "Test error message";
    private static final String RETHROW_MESSAGE = "Rethrow exception";
    private static final Exception RETHROW_EXCEPTION = new Exception(RETHROW_MESSAGE);

    @Test
    public void shouldBeMessageNullAndCauseNullWhenUsingDefaultConstructor() {
        final NativeLoaderException exception = new NativeLoaderException();

        assertNull(exception.getMessage());
        assertNull(exception.getCause());
    }

    @Test
    public void shouldBeMessageSetAndCauseNullWhenUsingConstructorWithMessage() {
        final NativeLoaderException exception = new NativeLoaderException(ERROR_MESSAGE);

        assertEquals(ERROR_MESSAGE, exception.getMessage());
        assertNull(exception.getCause());
    }

    @Test
    public void shouldBeMessageSetAndCauseSetWhenUsingConstructorWithMessageAndCause() {
        final NativeLoaderException exception = new NativeLoaderException(ERROR_MESSAGE, RETHROW_EXCEPTION);

        assertEquals(ERROR_MESSAGE, exception.getMessage());
        assertEquals(RETHROW_EXCEPTION, exception.getCause());
    }

    @Test
    public void shouldBeMessageSetAndCauseSetWhenUsingConstructorWithMessageAndCauseNull() {
        final NativeLoaderException exception = new NativeLoaderException(ERROR_MESSAGE, null);

        assertEquals(ERROR_MESSAGE, exception.getMessage());
        assertNull(exception.getCause());
    }

    @Test
    public void shouldBeMessageSetAndCauseSetWhenUsingConstructorWithCause() {
        final NativeLoaderException exception = new NativeLoaderException(RETHROW_EXCEPTION);

        assertEquals("java.lang.Exception: " + RETHROW_MESSAGE, exception.getMessage());
        assertEquals(RETHROW_EXCEPTION, exception.getCause());
    }

    @Test
    public void shouldBeMessageSetAndCauseSetWhenUsingConstructorWithCauseNull() {
        final NativeLoaderException exception = new NativeLoaderException((Throwable)null);

        assertNull(exception.getMessage());
        assertNull(exception.getCause());
    }
}