package eu.galjente.jni;

/**
 * {@link NativeLoaderException} are unchecked exception, it throws in case there is errors in NativeLoader.
 */
public class NativeLoaderException extends RuntimeException {

    /**
     * Constructs a new native loader exception.
     */
    public NativeLoaderException() {}

    /**
     * Constructs new native loader exception with the specified detail message.
     *
     * @param message the detail message.
     */
    public NativeLoaderException(String message) {
        super(message);
    }

    /**
     * Constructs new native loader exception with the specified detail message and cause.
     *
     * @param message the detail message.
     * @param cause the cause (A null value is permitted).
     */
    public NativeLoaderException(String message, Throwable cause) {
        super(message, cause);
    }

    /**
     * Constructs new native loader exception with specified cause and a detail message cause (cause == null ? null : cause.toString()).
     *
     * @param cause the cause (A null value is permitted).
     */
    public NativeLoaderException(Throwable cause) {
        super(cause);
    }

}
