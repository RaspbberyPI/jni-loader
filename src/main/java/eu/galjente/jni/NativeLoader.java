package eu.galjente.jni;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

/**
 * <p>{@link NativeLoader} loads native library from file system or extract from jar and then load it.
 * It's use 2 ways of native library load.</p>
 * <ul>
 *     <li>Search library in provided <b>-Djava.library.path=~/path</b></li>
 *     <li>Extract library fom JAR and load it</li>
 * </ul>
 *
 * <p>Example:</p>
 * <pre>{@code
 *     NativeLoader.create("native_lib")
 *                 .load();
 * }</pre>
 */
@Slf4j
public class NativeLoader {

    private final String name;
    private Path extLibraryDir = Path.of("lib");
    private String jarLibraryPath = "";
    private boolean checkFileHash = true;

    /**
     * Create {@link NativeLoader} with native library name.
     *
     * @param name native library name.
     * @return instance of {@link NativeLoader}.
     */
    public static NativeLoader create(String name) {
        return new NativeLoader(name);
    }

    /**
     * Create {@link NativeLoader} with native library name.
     *
     * @param name native library name.
     */
    private NativeLoader(String name) {
        this.name = name;
        log.trace("Native loader created \"{}\"", name);
    }

    /**
     * Set external library location where loader should store extracted native library (default: <b>lib</b>).
     *
     * @param path external library path.
     * @return {@link NativeLoader}.
     */
    public NativeLoader extLibraryDir(Path path) {
        this.extLibraryDir = path;
        log.trace("External library path set to \"{}\"", path);
        return this;
    }

    /**
     * Set native library location inside JAR file (default: <b>/</b>).
     * This path will be used by method {@link Class#getResourceAsStream(String)}.
     *
     * @param path native library path inside JAR file.
     * @return {@link NativeLoader}.
     */
    public NativeLoader jarLibraryPath(String path) {
        this.jarLibraryPath = path;
        log.trace("JAR library path set to \"{}\"", path);
        return this;
    }

    /**
     * Enable or disable file checksum comparing (default: <b>true</b>).
     * File checksum comparing required to avoid useless file extracting from JAR.
     *
     * @param check Enable(true)/Disable(false) checksum comparing.
     * @return {@link NativeLoader}
     */
    public NativeLoader checkFileChecksum(boolean check) {
        this.checkFileHash = check;
        log.trace("Check file checksum \"{}\"", check);
        return this;
    }

    /**
     * Load native library using method {@link System#loadLibrary(String)} and if it failed then
     * application start searching native library in JAR file and trying extract it.
     * If library extracted successfully than application try load it using method {@link System#loadLibrary(String)}.
     *
     * @exception NativeLoaderException if failed copy library or verify error.
     * @exception UnsatisfiedLinkError  if either the filename is not an absolute path name, the native library is not
     *                                  statically linked with the VM, or the library cannot be mapped to a native library image by the host system.
     */
    public void load() {
        try {
            log.trace("Loading system library \"{}\"", name);
            System.loadLibrary(name);
            log.info("System library \"{}\" loaded", name);
        } catch (UnsatisfiedLinkError e) {
            final Path libFilePath = getLibraryFilePath();
            log.info("System library \"{}\" not found, extracting library from JAR to \"{}\"", name, libFilePath);
            extractIfNeeded(libFilePath);
            System.load(libFilePath.toString());
            log.info("Library \"{}\" loaded", name);
        }
    }

    /**
     * For now {@link NativeLoader} support only linux system and this method return only "<b>so</b>" extension.
     *
     * @return native library extension.
     */
    private String getLibraryExtension() {
        return "so";
    }

    /**
     * Generating library file name based on provided <b>name</b> and file extension provided by {@link NativeLoader#getLibraryExtension()}.
     * File name example <b>library.so</b>
     * @return generated file name with extension.
     */
    private String getLibraryFileName() {
        return String.format("%s.%s", name, getLibraryExtension());
    }

    /**
     * Generate checksum file name based on provided name and extension "<b>sha256</b>".
     * File name example <b>library.sha256</b>.
     *
     * @return generated checksum file name with extension
     */
    private String getChecksumFileName() {
        return String.format("%s.sha256", name);
    }

    /**
     * Generate external library file path.
     *
     * @return Absolute file path to external library fail.
     */
    private Path getLibraryFilePath() {
        return extLibraryDir.resolve(getLibraryFileName()).toAbsolutePath();
    }

    /**
     * Create all required directories if needed.
     *
     * @param destinationDir destination directory
     * @exception NativeLoaderException thrown if failed create directory
     */
    private void prepareDirectories(Path destinationDir) {
        try {
            if (!Files.exists(destinationDir)) {
                Files.createDirectories(destinationDir);
                log.debug("Directory \"{}\" created", destinationDir);
            }
        } catch (IOException e) {
            log.error("Failed create directory \"{}\": {}", destinationDir, e);
            throw new NativeLoaderException(e);
        }
    }

    /**
     * Copy library from Jar to external location and override if library exist in external location.
     *
     * @param source library location inside JAR.
     * @param destinationJar native library file destination path.
     *
     * @exception NativeLoaderException thrown when library not found or failed to copy library
     */
    private void copyLibraryFromJar(String source, Path destinationJar) {
        try (InputStream in = NativeLoader.class.getResourceAsStream(source)) {
            if (in == null) {
                throw new NativeLoaderException("Library file \"" + source + "\" not found in JAR");
            }
            Files.copy(in, destinationJar.toAbsolutePath(), StandardCopyOption.REPLACE_EXISTING);
            log.debug("Library \"{}\" copied to \"{}\"", source, destinationJar);
        } catch (IOException e) {
            log.error("Failed copy library", e);
            throw new NativeLoaderException(e);
        }
    }

    /**
     * Calculate external native library file checksum with <b>SHA-256</b> hashing algorithm.
     *
     * @param extFilePath native library external path.
     * @return <b>SHA-256</b> hash.
     */
    private String getExtFileChecksum(Path extFilePath) {
        try (FileSha256ChecksumCalculator calculator = new FileSha256ChecksumCalculator(extFilePath.toString())) {
            final String checksum = calculator.calculate();
            log.debug("External library file \"{}\" checksum is \"{}\"", extFilePath, checksum);
            return checksum;
        } catch (Exception e) {
            log.error("Failed calculate external library file \"{}\" checksum", extFilePath);
            throw new NativeLoaderException("Failed calculate file \"" + extFilePath.toString() + "\" checksum: " + e.getMessage());
        }
    }

    /**
     * Read value from file of precalculated native library checksum.
     *
     * @param checksumFileName native library checksum file path inside JAR.
     * @return <b>SHA-256</b> hash.
     */
    private String getJarLibraryChecksum(String checksumFileName) {
        try (InputStream in = NativeLoader.class.getResourceAsStream(checksumFileName)) {
            if (in == null) {
                log.error("Checksum file \"{}\" not found", checksumFileName);
                throw new NativeLoaderException("Checksum file \"" + checksumFileName + "\" not found");
            }
            final String checksum = new String(in.readAllBytes());
            log.debug("Jar file library checksum \"{}\"", checksum);
            return checksum;
        } catch (IOException e) {
            log.error("Failed get jar file library checksum: {}", e.getMessage());
            throw new NativeLoaderException(e);
        }
    }

    /**
     * Comparing external library file checksum with the precalculated checksum located in JAR.
     *
     * @param extJarFilePath external library file path.
     * @return <b>true</b> if checksum equals, <b>false</b> if external file missing or checksums are different.
     */
    private boolean verifyChecksum(Path extJarFilePath) {
        boolean checksumEquals = false;
        log.debug("Verifying file checksums");
        if (checkFileHash && Files.exists(extJarFilePath)) {
            final String jarFileChecksumPath = jarLibraryPath + "/" + getChecksumFileName();
            final String jarFileChecksum = getJarLibraryChecksum(jarFileChecksumPath);
            final String extFileChecksum = getExtFileChecksum(extJarFilePath);
            checksumEquals = jarFileChecksum.equals(extFileChecksum);
            log.debug("JAR checksum \"{}\" and external file checksum \"{}\" are equals \"{}\"", jarFileChecksum, extFileChecksum, checksumEquals);
        } else {
            log.debug("Checksum verification skipped");
        }

        return checksumEquals;
    }

    /**
     * Method will verify external library file path and if required extract library from JAR to external library.
     *
     * @param libFilePath external library file path.
     */
    private void extractIfNeeded(Path libFilePath) {
        final String jarFilePath = jarLibraryPath + "/" + getLibraryFileName();
        final boolean checksumEquals = verifyChecksum(libFilePath);

        if (checksumEquals) {
            log.debug("Library files are equals, skipping extracting");
        } else {
            log.debug("Library files are different, extracting library from JAR");
            prepareDirectories(libFilePath.getParent());
            copyLibraryFromJar(jarFilePath, libFilePath);
        }
    }

}
