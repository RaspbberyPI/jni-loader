package eu.galjente.jni;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * <p>This {@link FileSha256ChecksumCalculator} class provide applications the functionality of a file digest algorithm, such as SHA-256.
 * {@link FileSha256ChecksumCalculator} obtain input bytes from a file in a file system. Reading stream and calculating checksum base on obtained baits.
 * For a calculating checksum is used {@link MessageDigest}, for file reading used {@link FileInputStream}.
 * A {@link FileSha256ChecksumCalculator} object starts out initialized. The data is processed through it using the {@link #calculate calculate} method.</p>
 *
 * <p>
 * To release resources used by this stream {@link #close} should be called
 * directly or by try-with-resources. Subclasses are responsible for the cleanup
 * of resources acquired by the subclass.
 * Subclasses that override {@link #finalize} in order to perform cleanup
 * should be modified to use alternative cleanup mechanisms such as
 * {@link java.lang.ref.Cleaner} and remove the overriding {@code finalize} method.</p>
 */
public class FileSha256ChecksumCalculator implements AutoCloseable {

    private final MessageDigest digest;
    private final FileInputStream inputStream;

    /**
     * Create <code>FileSha256ChecksumCalculator</code> by creating {@link FileInputStream} and providing name to it and creating
     * {@link MessageDigest} with <i>SHA-256</i> algorithm.
     *
     * @param name the system-dependent file name.
     * @throws NoSuchAlgorithmException if no {@code Provider} supports a {@code MessageDigestSpi} implementation for the specified algorithm.
     * @exception  FileNotFoundException if the file does not exist, is a directory rather than a regular file, or for some other reason cannot be opened for reading.
     * @exception  SecurityException if a security manager exists and its <code>checkRead</code> method denies read access to the file.
     */
    public FileSha256ChecksumCalculator(String name) throws NoSuchAlgorithmException, FileNotFoundException {
        this.digest = MessageDigest.getInstance("SHA-256");
        this.inputStream = new FileInputStream(name);
    }

    /**
     * Read a bytes from the file and updates the message digest.
     * When all bytes are read from file then called {@link MessageDigest#digest() digest} to completes the hash computation.
     * Final step is converting byte array to HEX string.
     *
     * @return file <i>SHA-256</i> checksum.
     * @exception IOException if an I/O error occurs.
     */
    public String calculate() throws IOException {
        final byte[] buffer = new byte[1024];
        int length;
        while ((length = inputStream.read(buffer)) > 0) {
            digest.update(buffer, 0, length);
        }

        final StringBuilder result = new StringBuilder();
        final byte[] values = digest.digest();
        for (byte value : values) {
            result.append(String.format("%02x", value));
        }
        return result.toString();
    }

    /**
     * Closes this file input stream and releases any system resources associated with the stream.
     *
     * @exception IOException if an I/O error occurs.
     */
    @Override
    public void close() throws Exception {
        inputStream.close();
    }
}
